<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Home;
use App\Http\Controllers\Login;
use App\Http\Controllers\LoginAdmin;
use App\Http\Controllers\ManageUser;
use App\Http\Controllers\Report;
use App\Http\Controllers\ManageCategory;
use App\Http\Controllers\ManageProduct;
use App\Http\Controllers\Register;
use App\Http\Controllers\HomeAdmin;
use App\Http\Controllers\Product;
use App\Http\Controllers\Cart;
use App\Http\Controllers\Profile;
use App\Http\Middleware\CheckAuth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Login::class, 'index']);
Route::get('/adm', [LoginAdmin::class, 'index']);
Route::get('/register', [Register::class, 'index']);
Route::get('/logout', [Login::class, 'logout']);
Route::get('/admin-logout', [LoginAdmin::class, 'logout']);
Route::post('/login-action', [Login::class, 'action']);
Route::post('/admin-login-action', [LoginAdmin::class, 'action']);
Route::post('/register-action', [Register::class, 'action']);

Route::group(['middleware' => ['check']], function () {

    Route::get('/home', [Home::class, 'index']);
    Route::get('/profile', [Profile::class, 'index']);
    Route::put('/update-profile', [Profile::class, 'update']);
    Route::get('/show-cart', [Cart::class, 'show']);
    Route::get('/checkout', [Cart::class, 'checkout']);
    Route::get('/store-cart/{product_id}', [Cart::class, 'store']);
    Route::get('/delete-cart/{product_id}', [Cart::class, 'delete']);
    Route::get('/product/{product_id}', [Product::class, 'index']);

    Route::get('/adm/transaction', [Report::class, 'index']);

    Route::get('/adm/transaction/{transaction_id}', [Report::class, 'detail']);

    Route::get('/adm/home', [HomeAdmin::class, 'index']);

    // user
    Route::get('/adm/user/create', [ManageUser::class, 'create']);
    Route::get('/adm/user/delete/{user_id}', [ManageUser::class, 'delete']);
    Route::get('/adm/user/edit/{user_id}', [ManageUser::class, 'edit']);
    Route::put('/adm/user/update/{user_id}', [ManageUser::class, 'update']);
    Route::post('/adm/user/store', [ManageUser::class, 'store']);
    Route::get('/adm/user', [ManageUser::class, 'index']);

    // category
    Route::get('/adm/category/create', [ManageCategory::class, 'create']);
    Route::get('/adm/category/delete/{category_id}', [ManageCategory::class, 'delete']);
    Route::get('/adm/category/edit/{category_id}', [ManageCategory::class, 'edit']);
    Route::put('/adm/category/update/{category_id}', [ManageCategory::class, 'update']);
    Route::post('/adm/category/store', [ManageCategory::class, 'store']);
    Route::get('/adm/category', [ManageCategory::class, 'index']);

    // product
    Route::get('/adm/product/create', [ManageProduct::class, 'create']);
    Route::get('/adm/product/delete/{product_id}', [ManageProduct::class, 'delete']);
    Route::get('/adm/product/edit/{product_id}', [ManageProduct::class, 'edit']);
    Route::put('/adm/product/update/{product_id}', [ManageProduct::class, 'update']);
    Route::post('/adm/product/store', [ManageProduct::class, 'store']);
    Route::get('/adm/product', [ManageProduct::class, 'index']);
});
