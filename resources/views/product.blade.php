@extends('template')

@section('content')
    <!-- Begin Li's Breadcrumb Area -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="breadcrumb-content">
                <ul>
                    <li><a href="{{ url('/home') }}">Home</a></li>
                    <li class="active">Single Product</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Li's Breadcrumb Area End Here -->
    <!-- content-wraper start -->
    <div class="content-wraper">
        <div class="container">
            <div class="row single-product-area">
                <div class="col-lg-5 col-md-6">
                    <!-- Product Details Left -->
                    <div class="product-details-left">
                        <div class="product-details-images slider-navigation-1">
                            <div class="lg-image">
                                <a class="popup-img venobox vbox-item" href="{{ asset('uploads').'/'.$product->image_url }}"
                                    data-gall="myGallery">
                                    <img src="{{ asset('uploads').'/'.$product->image_url }}" alt="product image">
                                </a>
                            </div>

                        </div>
                        <div class="product-details-thumbs slider-thumbs-1">
                            <div class="sm-image"><img src="{{ asset('uploads').'/'.$product->image_url }}" alt="product image thumb">
                            </div>
                        </div>
                    </div>
                    <!--// Product Details Left -->
                </div>

                <div class="col-lg-7 col-md-6">
                    <div class="product-details-view-content pt-60">
                        <div class="product-info">
                            <h2>{{ $product->name }}</h2>
                            <div class="rating-box pt-20">
                                <ul class="rating rating-with-review-item">
                                    @for ($i = 0; $i < $product->rating; $i++)
                                        <li><i class="fa fa-star-o"></i></li>
                                    @endfor
                                    @for ($i = 0; $i < 5 - $product->rating; $i++)
                                        <li class="no-star"><i class="fa fa-star-o"></i></li>
                                    @endfor

                                </ul>
                            </div>
                            <div class="price-box pt-20">
                                <span class="new-price new-price-2">Rp{{ $product->price }}</span>
                            </div>
                            <div class="product-desc">
                                <p>
                                    <span> {{ $product->description }}</span>

                                </p>
                            </div>

                            <div class="single-add-to-cart">
                                <form action="{{ url('store-cart').'/'.$product->id }}" method="get" class="cart-quantity">
                                    {{-- <div class="quantity">
                                        <label>Quantity</label>
                                        <div class="cart-plus-minus">
                                            <input class="cart-plus-minus-box" value="1" type="text">
                                            <div class="dec qtybutton"><i class="fa fa-angle-down"></i></div>
                                            <div class="inc qtybutton"><i class="fa fa-angle-up"></i></div>
                                        </div>
                                    </div> --}}
                                    <button class="add-to-cart" type="submit">Add to cart</button>
                                </form>
                                {{-- <a href="{{ url('store-cart').'/'.$product->id }}">
                                    <button class="add-to-cart" type="submit">Add to cart</button>
                                </a> --}}

                            </div>

                            <div class="block-reassurance">
                                <ul>
                                    <li>
                                        <div class="reassurance-item">
                                            <div class="reassurance-icon">
                                                <i class="fa fa-check-square-o"></i>
                                            </div>
                                            <p>Security policy (edit with Customer reassurance module)</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="reassurance-item">
                                            <div class="reassurance-icon">
                                                <i class="fa fa-truck"></i>
                                            </div>
                                            <p>Delivery policy (edit with Customer reassurance module)</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="reassurance-item">
                                            <div class="reassurance-icon">
                                                <i class="fa fa-exchange"></i>
                                            </div>
                                            <p> Return policy (edit with Customer reassurance module)</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- content-wraper end -->
@endsection
