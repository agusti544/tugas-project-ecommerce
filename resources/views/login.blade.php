@extends('template_login')

@section('content')
<!-- login area start -->
<div class="login-area">
    <div class="container">
        <div class="login-box ptb--100">
            <form action="{{ url('login-action') }}" method="post">
                @csrf
                <div class="login-form-head">
                    <h4>Sign In</h4>
                    {{-- <p>Hello there, Sign in and start managing your Admin Template</p> --}}
                </div>
                @if(Session::has('success')) 
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('success') }}
                    </div>
                
                @endif

                @if(Session::has('error')) 
                    <div class="alert alert-danger" role="alert">
                        {{ Session::get('error') }}
                    </div>
                
                @endif
                <div class="login-form-body">
                    <div class="form-gp">
                        <label for="exampleInputEmail1">Email address</label>
                        <input name="email" type="email" id="exampleInputEmail1" required>
                        <i class="ti-email"></i>
                        <div class="text-danger"></div>
                    </div>
                    <div class="form-gp">
                        <label for="exampleInputPassword1">Password</label>
                        <input name="password" type="password" id="exampleInputPassword1" required>
                        <i class="ti-lock"></i>
                        <div class="text-danger"></div>
                    </div>
                    <div class="row mb-4 rmber-area">
                        
                       
                    </div>
                    <div class="submit-btn-area">
                        <button id="form_submit" type="submit">Submit <i class="ti-arrow-right"></i></button>
                        
                    </div>
                    <div class="form-footer text-center mt-5">
                        <p class="text-muted">Don't have an account? <a href="{{ url('/register') }}">Sign up</a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- login area end -->
@endsection