@extends('template_login')

@section('content')

<!-- login area start -->
 <div class="login-area">
    <div class="container">
        
        <div class="login-box ptb--100">
            
            <form action="{{ url('/register-action') }}" method="post">
                @csrf
                <div class="login-form-head">
                    <h4>Sign up</h4>
                    <p>Hello there, Sign up and Join with Us</p>

                </div>
                @if(Session::has('success')) 
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('success') }}
                    </div>
                
                @endif

                @if(Session::has('error')) 
                    <div class="alert alert-danger" role="alert">
                        {{ Session::get('error') }}
                    </div>
                
                @endif
                <div class="login-form-body">
                    <div class="form-gp">
                        <label for="exampleInputName1">First Name</label>
                        <input name="first_name" type="text" id="exampleInputName1" required>
                        <i class="ti-user"></i>
                        <div class="text-danger"></div>
                    </div>
                    <div class="form-gp">
                        <label for="exampleInputName1">Last Name</label>
                        <input name="last_name" type="text" id="exampleInputName1" required>
                        <i class="ti-user"></i>
                        <div class="text-danger"></div>
                    </div>
                    <div class="form-gp">
                        <label for="exampleInputEmail1">Email address</label>
                        <input name="email" type="email" id="exampleInputEmail1" required>
                        <i class="ti-email"></i>
                        <div class="text-danger"></div>
                    </div>
                    <div class="form-gp">
                        <label for="exampleInputPassword1">Password</label>
                        <input name="password" type="password" id="exampleInputPassword1" required>
                        <i class="ti-lock"></i>
                        <div class="text-danger"></div>
                    </div>
                    
                    <div class="form-gp">
                        <label for="exampleInputName1">Country</label>
                        <input name="country" type="text" id="exampleInputName1" required>
                        {{-- <i class="ti-user"></i> --}}
                        <div class="text-danger"></div>
                    </div>
                    <div class="form-gp">
                        <label for="exampleInputName1">Address</label>
                        <input name="address" type="text" id="exampleInputName1" required>
                        {{-- <i class="ti-user"></i> --}}
                        <div class="text-danger"></div>
                    </div>
                    <div class="form-gp">
                        <label for="exampleInputName1">City</label>
                        <input name="city" type="text" id="exampleInputName1" required>
                        {{-- <i class="ti-user"></i> --}}
                        <div class="text-danger"></div>
                    </div>
                    <div class="form-gp">
                        <label for="exampleInputName1">State</label>
                        <input name="state" type="text" id="exampleInputName1" required>
                        {{-- <i class="ti-user"></i> --}}
                        <div class="text-danger"></div>
                    </div>
                    <div class="form-gp">
                        <label for="exampleInputName1">Postcode</label>
                        <input name="postcode" type="text" id="exampleInputName1" required>
                        {{-- <i class="ti-user"></i> --}}
                        <div class="text-danger"></div>
                    </div>
                    <div class="form-gp">
                        <label for="exampleInputName1">Telepon</label>
                        <input name="phone" type="text" id="exampleInputName1" required>
                        {{-- <i class="ti-user"></i> --}}
                        <div class="text-danger"></div>
                    </div>
                    <div class="submit-btn-area">
                        <button id="form_submit" type="submit">Submit <i class="ti-arrow-right"></i></button>
                    </div>
                    <div class="form-footer text-center mt-5">
                        <p class="text-muted">Don't have an account? <a href="{{ url('/') }}">Sign in</a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- login area end -->

@endsection