@extends('admin/template')

@section('content')

<div class="main-content-inner">
    <div class="row">
        <!-- data table start -->
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">

                    <a href="{{ url('/adm/transaction') }}">
                        <button class="btn btn-danger">
                            Kembali
                        </button>
                    </a>

                    <div class="mt-5 mb-5 font-bold">
                        Id Transaksi : {{$transaction->id }} <br>
                        User : {{$transaction->users->first_name.' '.$transaction->users->last_name }} <br>
                        Total : {{$transaction->total }} <br>
                        Status : {{$transaction->is_paid == 1 ? 'Sudah dibayar' : 'Belum dibayar' }} <br>
                    </div>

                    <h4 class="header-title">Data Table Detail Transaction</h4>
                    <div class="data-tables">
                        <table id="dataTable" class="text-center">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th>Product Name</th>
                                    <th>Quantity</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($detail_transactions as $detail_transaction)
                                <tr>
                                    <td>{{$detail_transaction->products->name}}</td>
                                    <td>{{$detail_transaction->quantity}}</td>
                                    <td>Rp{{$detail_transaction->subtotal}}</td>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- data table end -->
        
    </div>
</div>

@endsection