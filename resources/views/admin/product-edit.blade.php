@extends('admin/template')

@section('content')
    <!-- Textual inputs start -->
    <div class="col-12 mt-5">
        <div class="card">
            <form action="{{ url('adm/product/update').'/'.$product->id  }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="card-body">
                    <h4 class="header-title">Form Product</h4>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Name</label>
                        <input class="form-control" type="text" name="name" value="{{ $product->name }}" id="example-text-input"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Description</label>
                        <input class="form-control" type="text" name="description" value="{{ $product->description }}" id="example-text-input"
                            required>
                    </div>

                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Category</label>
                        <select name="category_id" id="" class="form-control" required>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}" {{ $category->id == $product->id ? 'selected' : '' }}>{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Type</label>
                        <select name="type" id="" class="form-control" required>
                            <option value="product" {{ 'product' == $product->type ? 'selected' : '' }}>Product</option>
                            <option value="service" {{ 'service' == $product->type ? 'selected' : '' }}>Service</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Image</label>
                        <img src="{{ asset('uploads').'/'.$product->image_url }}" alt="">
                        <input class="form-control" accept="image/x-png,image/gif,image/jpeg" type="file" name="image" value="" id="example-text-input"
                            >
                    </div>

                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Quantity</label>
                        <input class="form-control" type="number" name="quantity" value="{{ $product->quantity }}" id="example-text-input"
                            required>
                    </div>

                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Price</label>
                        <input class="form-control" type="number" name="price" value="{{ $product->price }}" id="example-text-input"
                            required>
                    </div>

                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Rating (0 - 5)</label>
                        <input class="form-control" type="number" min="0" max="5" name="rating" value="{{ $product->rating }}" id="example-text-input"
                            required>
                    </div>


                    <div class="form-group mt-5 has-danger">
                        <button class="btn btn-primary btn-block">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Textual inputs end -->
@endsection
