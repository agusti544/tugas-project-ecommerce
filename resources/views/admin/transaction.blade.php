@extends('admin/template')

@section('content')

<div class="main-content-inner">
    <div class="row">
        <!-- data table start -->
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Table Transaction</h4>
                    <div class="data-tables">
                        <table id="dataTable" class="text-center">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th>Name</th>
                                    <th>Total</th>
                                    <th>Is Paid</th>
                                    <th>Created</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($transactions as $transaction)
                                <tr>
                                    <td>{{$transaction->users->first_name.' '.$transaction->users->last_name}}</td>
                                    <td>Rp{{$transaction->total}}</td>
                                    <td>{{$transaction->users->first_name == 1 ? 'Sudah' : 'Belum'}}</td>
                                    <td>{{$transaction->users->created_at}}</td>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- data table end -->
        
    </div>
</div>

@endsection