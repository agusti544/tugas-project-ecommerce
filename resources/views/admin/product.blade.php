@extends('admin/template')

@section('content')
    <div class="main-content-inner">
        <div class="row">
            <!-- data table start -->
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">Data Table Product</h4>
                        <a href="{{ url('adm/product/create') }}">
                            <button class="btn btn-primary mb-3 float-right">Tambah Product</button>
                        </a>
                        <div class="data-tables">
                            <table id="dataTable" class="text-center">
                                <thead class="bg-light text-capitalize">
                                    <tr>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Description</th>
                                        <th>Type</th>
                                        <th>Image</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Rating</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($products as $product)
                                        <tr>
                                            <td>{{ $product->name }}</td>
                                            <td>{{ $product->categories->name }}</td>
                                            <td>{{ $product->description }}</td>
                                            <td>{{ $product->type }}</td>
                                            <td> <img src="{{ asset('uploads').'/'.$product->image_url }}" alt="{{$product->name}}"> </td>
                                            <td>{{ $product->quantity }}</td>
                                            <td>{{ $product->price }}</td>
                                            <td>{{ $product->rating }}</td>
                                          
                                            <td>

                                                <a href="{{ url('adm/product/edit').'/'.$product->id }}">
                                                    <button class="btn btn-warning m-2">Edit</button>
                                                </a>
                                                <a href="{{ url('adm/product/delete').'/'.$product->id }}">
                                                    <button class="btn btn-danger mb-2">Delete</button>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- data table end -->

        </div>
    </div>
@endsection
