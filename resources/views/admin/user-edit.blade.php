@extends('admin/template')

@section('content')
    <!-- Textual inputs start -->
    <div class="col-12 mt-5">
        <div class="card">
            <form action="{{ url('adm/user/update').'/'.$user->id }}" method="post">
                @csrf
                @method('put')
                <div class="card-body">
                    <h4 class="header-title">Form User</h4>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">First Name</label>
                        <input class="form-control" type="text" name="first_name" value="{{ $user->first_name }}" id="example-text-input"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Last Name</label>
                        <input class="form-control" type="text" name="last_name" value="{{ $user->first_name }}" id="example-text-input"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="example-email-input" class="col-form-label">Email</label>
                        <input class="form-control" type="email" name="email" value="{{ $user->email }}" id="example-email-input"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Password</label>
                        <input class="form-control" type="password" name="password" value="{{ $user->password }}" id="example-text-input"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Phone</label>
                        <input class="form-control" type="text" name="phone" value="{{ $user->phone }}" id="example-text-input"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Address</label>
                        <input class="form-control" type="text" name="address" value="{{ $user->address }}" id="example-text-input"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Country</label>
                        <input class="form-control" type="text" name="country" value="{{ $user->country }}" id="example-text-input"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">City</label>
                        <input class="form-control" type="text" name="city" value="{{ $user->city }}" id="example-text-input"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">State</label>
                        <input class="form-control" type="text" name="state" value="{{ $user->state }}" id="example-text-input"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">postcode</label>
                        <input class="form-control" type="text" name="postcode" value="{{ $user->postcode }}" id="example-text-input"
                            required>
                    </div>

                    <div class="form-group mt-5 has-danger">
                        <button class="btn btn-primary btn-block">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Textual inputs end -->
@endsection
