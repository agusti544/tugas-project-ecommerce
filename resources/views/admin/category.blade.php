@extends('admin/template')

@section('content')
    <div class="main-content-inner">
        <div class="row">
            <!-- data table start -->
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">Data Table Category</h4>
                        <a href="{{ url('adm/category/create') }}">
                            <button class="btn btn-primary mb-3 float-right">Tambah Category</button>
                        </a>
                        <div class="data-tables">
                            <table id="dataTable" class="text-center">
                                <thead class="bg-light text-capitalize">
                                    <tr>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($categories as $category)
                                        <tr>
                                            <td>{{ $category->name }}</td>
                                            <td>{{ $category->description }}</td>
                                          
                                            <td>

                                                <a href="{{ url('adm/category/edit').'/'.$category->id }}">
                                                    <button class="btn btn-warning m-2">Edit</button>
                                                </a>
                                                <a href="{{ url('adm/category/delete').'/'.$category->id }}">
                                                    <button class="btn btn-danger mb-2">Delete</button>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- data table end -->

        </div>
    </div>
@endsection
