@extends('admin/template')

@section('content')

<div class="main-content-inner">
    <div class="row">
        <!-- data table start -->
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Table Transaction</h4>
                    <div class="data-tables">
                        <table id="dataTable" class="text-center">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th>Name</th>
                                    <th>Total</th>
                                    <th>Is Paid</th>
                                    <th>Created</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($transactions as $transaction)
                                <tr>
                                    <td>{{$transaction->users->first_name.' '.$transaction->users->last_name}}</td>
                                    <td>Rp{{$transaction->total}}</td>
                                    <td>{{$transaction->is_paid == 1 ? 'Sudah' : 'Belum'}}</td>
                                    <td>{{$transaction->created_at}}</td>
                                    <td>

                                        <a href="{{ url('adm/transaction').'/'.$transaction->id }}">
                                            <button class="btn btn-primary m-2">Detail</button>
                                        </a>

                                    </td>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- data table end -->
        
    </div>
</div>

@endsection