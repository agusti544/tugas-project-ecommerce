@extends('admin/template')

@section('content')
    <!-- Textual inputs start -->
    <div class="col-12 mt-5">
        <div class="card">
            <form action="{{ url('adm/category/update').'/'.$category->id }}" method="post">
                @csrf
                @method('put')
                <div class="card-body">
                    <h4 class="header-title">Form Category</h4>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Name</label>
                        <input class="form-control" type="text" value="{{ $category->name }}" name="name" value="" id="example-text-input"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Description</label>
                        <input class="form-control" type="text" value="{{ $category->description }}" name="description" value="" id="example-text-input"
                            required>
                    </div>
                    

                    <div class="form-group mt-5 has-danger">
                        <button class="btn btn-primary btn-block">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Textual inputs end -->
@endsection
