@extends('template')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <form action="{{ url('update-profile') }}" method="post">
                    @method('put')
                    @csrf
                    <div class="checkbox-form">
                        <h3>Billing Details</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="country-select clearfix">
                                    <label>Country <span class="required">*</span></label>
                                    <input placeholder="" name="country" value="{{ $user->country }}" type="text">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="checkout-form-list">
                                    <label>First Name <span class="required">*</span></label>
                                    <input placeholder="" name="first_name" value="{{ $user->first_name }}" type="text">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="checkout-form-list">
                                    <label>Last Name <span class="required">*</span></label>
                                    <input placeholder="" name="last_name" value="{{ $user->last_name }}" type="text">
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="checkout-form-list">
                                    <label>Address <span class="required">*</span></label>
                                    <input placeholder="Street address" name="address" value="{{ $user->address }}" type="text">
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="checkout-form-list">
                                    <label>Town / City <span class="required">*</span></label>
                                    <input type="text" name="city" value="{{ $user->city }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="checkout-form-list">
                                    <label>State / County <span class="required">*</span></label>
                                    <input placeholder="" name="state" value="{{ $user->state }}" type="text">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="checkout-form-list">
                                    <label>Postcode / Zip <span class="required">*</span></label>
                                    <input placeholder="" name="postcode" value="{{ $user->postcode }}" type="text">
                                </div>
                            </div>
                           
                            <div class="col-md-12">
                                <div class="checkout-form-list">
                                    <label>Phone <span class="required">*</span></label>
                                    <input name="phone" value="{{ $user->phone }}" type="text">
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="checkout-form-list">
                                   <button type="submit" class="btn btn-primary btn-block">Update</button>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </form>
            </div>
           
        </div>
    </div>
@endsection
