@extends('template')

@section('content')
    <!-- Begin Product Area -->
    <div class="product-area pt-60 pb-50">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="li-product-tab">
                        <ul class="nav li-product-menu">
                            <li><a class="active" data-toggle="tab" href="#li-new-product"><span>Produk</span></a></li>
                            <li><a data-toggle="tab" href="#li-bestseller-product"><span>Jasa</span></a></li>
                        </ul>
                    </div>
                    <!-- Begin Li's Tab Menu Content Area -->
                </div>
            </div>
            <div class="tab-content">
                <div id="li-new-product" class="tab-pane active show" role="tabpanel">
                    <div class="row">
                        <div class="product-active owl-carousel">
                            <div class="col-lg-12">
                                @foreach ($products as $product)
                                    <!-- single-product-wrap start -->
                                    <div class="single-product-wrap">
                                        <div class="product-image">
                                            <a href="{{ url('/product').'/'.$product->id }}">
                                                <img src="{{ asset('uploads').'/'.$product->image_url }}" alt="{{ $product->name }}">
                                            </a>
                                            {{-- <span class="sticker">New</span> --}}
                                        </div>
                                        <div class="product_desc">
                                            <div class="product_desc_info">
                                                <div class="product-review">
                                                    <h5 class="manufacturer">
                                                        {{-- <a href="shop-left-sidebar.html">Graphic Corner</a> --}}
                                                    </h5>
                                                    <div class="rating-box">
                                                        <ul class="rating">
                                                            @for ($i = 0; $i < $product->rating; $i++)
                                                                <li><i class="fa fa-star-o"></i></li>
                                                            @endfor
                                                            @for ($i = 0; $i < 5 - $product->rating; $i++)
                                                                <li class="no-star"><i class="fa fa-star-o"></i></li>
                                                            @endfor
                                                        </ul>
                                                    </div>
                                                </div>
                                                <h4><a class="product_name"
                                                        href="{{ url('/product').'/'.$product->id }}">{{ $product->name }}</a></h4>
                                                <div class="price-box">
                                                    <span class="new-price">Rp{{ $product->price }}</span>
                                                </div>
                                            </div>
                                            <div class="add-actions">
                                                <ul class="add-actions-link">
                                                    <li class="add-cart active"><a href="{{ url('store-cart').'/'.$product->id }}">Add to cart</a></li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- single-product-wrap end -->
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>
                <div id="li-bestseller-product" class="tab-pane" role="tabpanel">
                    <div class="row">
                        <div class="product-active owl-carousel">
                            <div class="col-lg-12">
                                @foreach ($services as $service)
                                <!-- single-product-wrap start -->
                                <div class="single-product-wrap">
                                    <div class="product-image">
                                        <a href="{{ url('/product').'/'.$service->id }}">
                                            <img src="{{ asset('uploads').'/'.$service->image_url }}" alt="{{ $service->name }}">
                                        </a>
                                        {{-- <span class="sticker">New</span> --}}
                                    </div>
                                    <div class="product_desc">
                                        <div class="product_desc_info">
                                            <div class="product-review">
                                                <h5 class="manufacturer">
                                                    {{-- <a href="shop-left-sidebar.html">Graphic Corner</a> --}}
                                                </h5>
                                                <div class="rating-box">
                                                    <ul class="rating">
                                                        @for ($i = 0; $i < $service->rating; $i++)
                                                            <li><i class="fa fa-star-o"></i></li>
                                                        @endfor
                                                        @for ($i = 0; $i < 5 - $service->rating; $i++)
                                                            <li class="no-star"><i class="fa fa-star-o"></i></li>
                                                        @endfor
                                                    </ul>
                                                </div>
                                            </div>
                                            <h4><a class="product_name"
                                                    href="{{ url('/product').'/'.$service->id }}">{{ $service->name }}</a></h4>
                                            <div class="price-box">
                                                <span class="new-price">Rp{{ $service->price }}</span>
                                            </div>
                                        </div>
                                        <div class="add-actions">
                                            <ul class="add-actions-link">
                                                <li class="add-cart active"><a href="{{ url('store-cart').'/'.$service->id }}">Add to cart</a></li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- single-product-wrap end -->
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Product Area End Here -->
@endsection
