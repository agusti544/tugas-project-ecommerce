@extends('template')

@section('content')

<!-- Error 404 Area Start -->
<div class="error404-area pt-30 pb-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="error-wrapper text-center ptb-50 pt-xs-20">
                    <div class="error-text">
                        <h2>Pesanan Berhasil</h2>
                    </div>
                   
                    <div class="error-button">
                        <a href="{{ url('home') }}">Kembali Belanja</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Error 404 Area End -->

@endsection