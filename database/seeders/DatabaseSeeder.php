<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        \App\Models\Admin::insert([
            'id' => 1,
            'first_name' => 'Admin',
            'last_name' => 'Ecommerce',
            'email' => 'admin@gmail.com',
            'password' => '123456',
        ]);

        \App\Models\User::insert([
            'id' => 1,
            'first_name' => 'User',
            'last_name' => 'Ecommerce',
            'email' => 'user@gmail.com',
            'country' => 'Indonesia',
            'address' => 'Jl. sudirman',
            'city' => 'Jakarta Pusat',
            'state' => 'DKI Jakarta',
            'postcode' => '40000',
            'phone' => '62822',
            'password' => '123456',
        ]);

        \App\Models\Category::insert([
            'id' => 1,
            'name' => 'Contoh',
            'description' => 'Ini contoh category',
        ]);

        \App\Models\Product::insert([
            'id' => 1,
            'name' => 'Produk satu',
            'type' => 'product',
            'image_url' => '1.jpg',
            'description' => 'Ini contoh p',
            'category_id' => 1,
            'quantity' => 20,
            'price' => 150000,
            'rating' => 3,
        ]);

        \App\Models\Product::insert([
            'id' => 2,
            'name' => 'Produk dua',
            'type' => 'service',
            'image_url' => '2.jpg',
            'description' => 'Ini contoh p',
            'category_id' => 1,
            'quantity' => 20,
            'price' => 200000,
            'rating' => 3,
        ]);

        \App\Models\Transaction::insert([
            'id' => 1,
            'user_id' => 1,
            'total' => 350000,
            'is_paid' => 1
        ]);

        \App\Models\Detail_transaction::insert([
            'id' => 1,
            'transaction_id' => 1,
            'product_id' => 1,
            'quantity' => 1,
            'subtotal' => 150000,
        ]);

        \App\Models\Detail_transaction::insert([
            'id' => 2,
            'transaction_id' => 1,
            'product_id' => 2,
            'quantity' => 1,
            'subtotal' => 200000,
        ]);
    }
}
