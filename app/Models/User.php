<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'country',
        'address',
        'city',
        'state',
        'postcode',
        'phone',
        'email',
        'password',
    ];

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }
}
