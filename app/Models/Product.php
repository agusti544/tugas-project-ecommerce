<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'type',
        'image_url',
        'category_id',
        'quantity',
        'price',
        'rating',
    ];

    public function categories()
    {
        // return $this->belongsTo('Model', 'foreign_key', 'owner_key'); 
        return $this->belongsTo('App\Models\Category','category_id','id');
    }

    public function detail_transactions()
    {
        return $this->hasMany('App\Models\Detail_transaction');
    }
}
