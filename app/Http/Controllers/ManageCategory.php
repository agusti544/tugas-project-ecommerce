<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;


class ManageCategory extends Controller
{
    function index(){
        $categories = Category::all();
        return view('admin/category', [
            'categories' => $categories
        ]);
    }

    function create(){
        return view('admin/category-create');
    }

    function store(Request $request){
        try {
            $data = $request->except(['_token']);
            // dd($data);
            Category::create($data);
            return redirect('adm/category');
        } catch (\Throwable $th) {
            //throw $th;
            dd($th);
            return redirect()->back()->with('error', 'Create Failed!');
        }
    }

    function edit(Request $request, $category_id){
        $category = Category::find($category_id);
        return view('admin/category-edit', [
            'category' => $category
        ]);
    }

    function update(Request $request, $category_id){
        try {
            $data = $request->except(['_token','_method']);
            // dd($data);
            Category::where('id', $category_id)->update($data);
            return redirect('adm/category');
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->back()->with('error', 'Delete Failed!');
        }
    }

    function delete(Request $request, $category_id){
        try {
            Category::where('id', $category_id)->delete();
            return redirect('adm/category');
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->back()->with('error', 'Delete Failed!');
        }
    }
}
