<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Product extends Controller
{
    function index(Request $request, $product_id){
        $product = \App\Models\Product::find($product_id);
        // dd($product_id);
        return view('product', [
            'product' => $product
        ]);
    }
}
