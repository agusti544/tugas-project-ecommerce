<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\Detail_transaction;
use Illuminate\Validation\ValidationException;

// $cart = [
//     'products' => [],
//     'total' => ''
// ];

class Cart extends Controller
{
    function show(Request $request)
    {
        dd($request->session()->get('cart'));
    }

    function store(Request $request, $product_id)
    {
        try {
            $product = Product::find($product_id);
            if (!$product) throw new ValidationException('Product not found');
            $isExist = false;
            $cart = $request->session()->get('cart');

            if ($cart != null) {
                foreach ($cart['products'] as $key => $item) {
                    if ($item['id'] == $product_id) $isExist = true;
                    if ($isExist) {
                        $cart['products'][$key]['quantity'] = $cart['products'][$key]['quantity'] + 1;
                    }
                }
            }

            if (!$isExist) {
                if($cart == null) $cart['products'] = [];
                array_push($cart['products'], [
                    'id' => $product->id,
                    'name' => $product->name,
                    'image_url' => $product->image_url,
                    'price' => $product->price,
                    'quantity' => 1,
                ]);
            }

            $total = 0;
            foreach ($cart['products'] as $key => $item) {
                $total += $item['price'] * $item['quantity'];
            }

            $cart['total'] = $total;
            // dd($cart);

            $request->session()->put('cart', $cart);
            return redirect('/home');
        } catch (\Throwable $th) {
            //throw $th;
            dd($th);
            return redirect()->back()->with('error', 'Failed!');
        }
    }

    function delete(Request $request, $product_id)
    {
        try {
            $product = Product::find($product_id);
            if (!$product) throw new ValidationException('Product not found');
            $isExist = false;
            $cart = $request->session()->get('cart');
            $filtered_products = [];
            foreach ($cart['products'] as $key => $item) {
                if ($item['id'] == $product_id) $isExist = true;
                if (!$isExist) {
                    array_push($filtered_products, $item);
                }
            }

            $cart['products'] = $filtered_products;

            $total = 0;
            foreach ($cart['products'] as $key => $item) {
                $total += $item['price'] * $item['quantity'];
            }

            $cart['total'] = $total;
            $request->session()->put('cart', $cart);
            return redirect('/home');
        } catch (\Throwable $th) {
            //throw $th;
            dd($th);
            return redirect()->back()->with('error', 'Failed!');
        }
    }

    function checkout(Request $request){
        try {
            if($request->session()->get('cart') == null) {
                return redirect()->back()->with('error', 'Keranjang kosong');
            }
            $auth = $request->session()->get('auth');
            $cart = $request->session()->get('cart');
            $transaction = Transaction::create([
                'user_id' => $auth->id,
                'total' => $cart['total'],
                'is_paid' => 1
            ]);
            foreach($cart['products'] as $product){
                Detail_transaction::create([
                    'transaction_id' => $transaction->id,
                    'product_id' => $product['id'],
                    'quantity' => $product['quantity'],
                    'subtotal' => $product['quantity'] * $product['price'] 
                ]);
            }
            $request->session()->put('cart', null);
            return view('success');
        } catch (\Throwable $th) {
            //throw $th;
            dd($th);
            return redirect()->back()->with('error', 'Failed!');
        }
    }
}
