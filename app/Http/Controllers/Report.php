<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Detail_transaction;

class Report extends Controller
{
    function index(Request $request){
        $transactions = Transaction::with(['users'])->get();
        return view('admin/home', [
            'transactions' => $transactions
        ]);
    }

    function detail(Request $request, $transaction_id){
        $transaction = Transaction::with(['users'])->where([
            'id' => $transaction_id
        ])->first();
        $detail_transactions = Detail_transaction::with(['products'])->where([
            'transaction_id' => $transaction_id
        ])->get();
        return view('admin/detail_transaction', [
            'transaction' => $transaction,
            'detail_transactions' => $detail_transactions
        ]);
    }
}
