<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Validation\ValidationException;

class Login extends Controller
{
    function index(){
        return view('login');
    }

    function action(Request $request){
        try {
            $data = $request->all();
            unset($data['_token']);
            $user = User::where($data)->first();
            // dd($user);
            if(!$user) throw new ValidationException('User tidak ditemukan');
            $request->session()->put('auth', $user);
            return redirect('/home')->with('success', 'Login Success!');
        } catch (\Throwable $th) {
            //throw $th;
            // dd($th);
            return redirect()->back()->with('error', 'Login Failed!');
        }
    }

    function logout(Request $request) {
        $request->session()->flush();
        return redirect('/');
    }
}
