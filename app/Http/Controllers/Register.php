<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class Register extends Controller
{
    function index(){
        return view('register');
    }

    function action(Request $request){
        try {
            $data = $request->all();
            User::create($data);
            return redirect('/')->with('success', 'Register Success!');
        } catch (\Throwable $th) {
            //throw $th;
            // dd($th);
            return redirect('register')->with('error', 'Register Failed!');
        }
    }
}
