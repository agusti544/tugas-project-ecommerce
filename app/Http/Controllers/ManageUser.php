<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class ManageUser extends Controller
{
    function index(){
        $users = User::all();
        return view('admin/user', [
            'users' => $users
        ]);
    }

    function create(){
        return view('admin/user-create');
    }

    function store(Request $request){
        try {
            $data = $request->all();
            unset($data['_token']);
            User::create($data);
            return redirect('adm/user');
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->back()->with('error', 'Create Failed!');
        }
    }

    function edit(Request $request, $user_id){
        $user = User::find($user_id);
        return view('admin/user-edit', [
            'user' => $user
        ]);
    }

    function update(Request $request, $user_id){
        try {
            $data = $request->except(['_token','_method']);
            // dd($data);
            User::where('id', $user_id)->update($data);
            return redirect('adm/user');
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->back()->with('error', 'Delete Failed!');
        }
    }

    function delete(Request $request, $user_id){
        try {
            User::where('id', $user_id)->delete();
            return redirect('adm/user');
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->back()->with('error', 'Delete Failed!');
        }
    }
}
