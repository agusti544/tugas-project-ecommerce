<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Validation\ValidationException;

class LoginAdmin extends Controller
{
    function index(){
        return view('admin/login');
    }

    function action(Request $request){
        try {
            $data = $request->all();
            unset($data['_token']);
            $user = Admin::where($data)->first();
            // dd($user);
            if(!$user) throw new ValidationException('Admin tidak ditemukan');
            $request->session()->put('auth', $user);
            return redirect('/adm/home')->with('success', 'Login Success!');
        } catch (\Throwable $th) {
            //throw $th;
            // dd($th);
            return redirect()->back()->with('error', 'Login Failed!');
        }
    }

    function logout(Request $request) {
        $request->session()->flush();
        return redirect('/adm');
    }
}
