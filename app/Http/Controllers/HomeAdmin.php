<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaction;

class HomeAdmin extends Controller
{
    function index(Request $request){
        $transactions = Transaction::with(['users'])->get();
        return view('admin/home', [
            'transactions' => $transactions
        ]);
    }
}