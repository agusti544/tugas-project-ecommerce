<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class Profile extends Controller
{
    function index(Request $request) {
        $auth = $request->session()->get('auth');
        return view('profile', [
            'user' => $auth
        ]);
    }

    function update(Request $request) {
        try {
            $auth = $request->session()->get('auth');
            $data = $request->all();
            unset($data['_method']);
            unset($data['_token']);
            User::where('id',$auth->id)->update($data);  
            $request->session()->put('auth', User::find($auth->id));  
            return redirect('profile');        
        } catch (\Throwable $th) {
            //throw $th;
            dd($th);
            return redirect()->back()->with('error', 'Update Failed!');
        }
    }
}
