<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;

class ManageProduct extends Controller
{
    function index()
    {
        $products = Product::with(['categories'])->get();
        // dd($products);
        return view('admin/product', [
            'products' => $products
        ]);
    }

    function create()
    {
        $categories = Category::all();
        return view('admin/product-create', [
            'categories' => $categories
        ]);
    }

    function store(Request $request)
    {
        try {
            $data = $request->except(['_token']);
            // dd($data);
            $validatedData = $request->validate([
                'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:8182',
            ]);

            $image = $request->image;
            $imageName = time() . '.' . $image->extension();

            // 1) Store in Storage folder -- (Path => storage/app/images/file.png)
            $image->storeAs('images', $imageName);

            // 2) Store in Public Folder -- (Path => public/images/file.png)
            $image->move(public_path('uploads'), $imageName);
            // dd($path);

            $data['image_url'] = $imageName;
            unset($data['image']);
            Product::create($data);
            return redirect('adm/product');
        } catch (\Throwable $th) {
            //throw $th;
            dd($th);
            return redirect()->back()->with('error', 'Create Failed!');
        }
    }

    function edit(Request $request, $product_id)
    {
        $categories = Category::all();
        $product = Product::find($product_id);
        return view('admin/product-edit', [
            'product' => $product,
            'categories' => $categories
        ]);
    }

    function update(Request $request, $product_id)
    {
        try {
            $data = $request->except(['_token', '_method']);
            // dd($data);
            if (array_key_exists('image', $data)) {
                $validatedData = $request->validate([
                    'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:8182',
                ]);

                $image = $request->image;
                $imageName = time() . '.' . $image->extension();
    
                // 1) Store in Storage folder -- (Path => storage/app/images/file.png)
                $image->storeAs('images', $imageName);
    
                // 2) Store in Public Folder -- (Path => public/images/file.png)
                $image->move(public_path('uploads'), $imageName);

                $data['image_url'] = $imageName;
                unset($data['image']);
                // dd($data);
            }
            Product::where('id', $product_id)->update($data);
            return redirect('adm/product');
        } catch (\Throwable $th) {
            //throw $th;
            dd($th);
            return redirect()->back()->with('error', 'Delete Failed!');
        }
    }

    function delete(Request $request, $product_id)
    {
        try {
            Product::where('id', $product_id)->delete();
            return redirect('adm/product');
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->back()->with('error', 'Delete Failed!');
        }
    }
}
