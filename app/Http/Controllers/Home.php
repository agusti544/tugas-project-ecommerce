<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Transaction;
use App\Models\Product;

class Home extends Controller
{
    function index(){
        $products = Product::where([
            'type' => 'product',
        ])->get();
        $services = Product::where([
            'type' => 'service',
        ])->get();
        return view('home', [
            'services' => $services,
            'products' => $products
        ]);
    }
}
